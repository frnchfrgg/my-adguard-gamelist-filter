# AdGuard_GameList-Filter

This compilation list is very useful for blocking domains related to online video games.
It was originally designed to control access to schools.

This is based on https://github.com/Mafraysse/AdGuard_GameList-Filter/tree/main

## Thanks
- https://github.com/Mafraysse/AdGuard_GameList-Filter/tree/main

Thanks of the original author:
- Toulouse 1 Capitole University : http://dsi.ut-capitole.fr/blacklists/ (Fabrice Prigent)
- Shub https://gitlab.com/5hub/games-blocklist/-/blob/master/filter.txt
- brandonlang2 (https://community.spiceworks.com/topic/1972247-web-filtering-in-a-high-school-my-experience-list-and-ps-script-included)
